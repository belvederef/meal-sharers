# README #


### What is this repository for? ###
Initial design. Includes state diagram, class diagram and acceptance tests.

"Meal-Sharers" brings together those who love to cook and share food (Cooks), with a local
older neighbour (Eater) who would really appreciate receiving a freshly prepared meal and a
friendly chat. Usually the Cook would be cooking a meal for their own family and would
make a bit more of everything to take to a local older neighbour. The aim is to help
communities to interact more and build lasting and supportive relationships between the
generations.
Volunteer Cooks sign up online, complete the food hygiene certification instruction and quiz.
They also undergo a PVG (criminal record) background check. Once their application has
been approved a Cook can select an Eater from a list of Eaters in their local area (filtered by
travel distance, and food preferences) and offer to share a meal with them. The Cook then
prepares and delivers a tasty home cooked meal to their Eater and uploads a picture of the
meal-share onto the system. The system also captures information about the date, time,
cook, and Eater. A Meal-Sharers administrator then phones both parties to hear how it
went, and then each parties comments 
