﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers
{
    class Cook
    {
        private HygieneCertificateStatus cookHygieneCertificate;
        private PVGStatus cookPVG;
        private int id;
        private String feedback;
        private Meal meal;

        public Cook()
        {
            cookHygieneCertificate = new HygieneCertificateStatus(DateTime.Parse("20/05/2017"));
            cookPVG = new PVGStatus();
        }

        public Cook getCookInfo()
        {
            return this;
        }
        public String getCookFeedback()
        {
            return feedback;
        }

        //Methods for the sign up page
        public HygieneCertificateStatus getHygieneCertificateStatus()
        {
            return cookHygieneCertificate;
        }

        public PVGStatus getPVGStatus()
        {
            return cookPVG;
        }
    }
}
