﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers
{
    class Eater
    {
        private int id;
        private Boolean eaterResponseToShare;
        private String feedback;

        public Eater() { }

        //This is also called retrieveEaterDetails on the sequence diagram - two names for the same thing
        public Eater getEaterInfo()
        {
            return this;
        }
        public Boolean offerToShare()
        {
            return eaterResponseToShare;
        }
        public void deliverMeal(Meal meal) { }

        public String getEaterFeedback()
        {
            return feedback;
        }
    }
}
