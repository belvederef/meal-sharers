﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Meal_Sharers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers.Tests
{
    [TestClass()]
    public class HygieneCertificateStatusTests
    {
        [TestMethod()]
        public void hygiene_certificate_OK_status_test()
        {
            //First test: the user completes the certificate. The user's status should be updated accordingly

            ////Arrange
            //Beginning status is always NONE
            HygieneCertificateStatus hygieneCertificateStatus = new HygieneCertificateStatus();
            String expected = "State of Hygiene Certificate = OK";

            ////Act
            hygieneCertificateStatus.eventClick();
        

            ////Assert
            Assert.AreEqual(expected, hygieneCertificateStatus.ToString(), "Incorrect status");
        }

        [TestMethod()]
        public void hygiene_certificate_positive_renew_status_test()
        {
            //Getting a notification 3 months before certificate expiration

            ////Arrange
            //Beginning status is always NONE
            HygieneCertificateStatus hygieneCertificateStatus = new HygieneCertificateStatus();

            //Add the cook's certificate to the system. Then check for its validity
            hygieneCertificateStatus.addCertificateExpiryDate(DateTime.Parse("20/05/2017"));
            String expected = "State of Hygiene Certificate = RENEW";

            ////Act
            // Going from the NONE status to OK, then RENEW
            hygieneCertificateStatus.eventClick();
            hygieneCertificateStatus.eventClick();


            ////Assert
            Assert.AreEqual(expected, hygieneCertificateStatus.ToString(), "Incorrect status");
        }

        [TestMethod()]
        public void hygiene_certificate_negative_renew_status_test()
        {
            //If today's date is not within the last 3 months of the certificate lifespan, do not send a notification

            ////Arrange
            //Beginning status is always NONE
            HygieneCertificateStatus hygieneCertificateStatus = new HygieneCertificateStatus();

            //Add the cook's certificate to the system. Then check for its validity
            hygieneCertificateStatus.addCertificateExpiryDate(DateTime.Parse("20/09/2017"));
            String expected = "State of Hygiene Certificate = OK";

            ////Act
            // Going from the NONE status to OK, then RENEW
            hygieneCertificateStatus.eventClick();
            hygieneCertificateStatus.eventClick();


            ////Assert
            Assert.AreEqual(expected, hygieneCertificateStatus.ToString(), "Incorrect status");
        }
    }
}