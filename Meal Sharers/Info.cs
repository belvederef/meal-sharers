﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers
{
    class Info
    {
        private DateTime date;

        public Info() { }

        public DateTime getDate()
        {
            return date.Date;
        }
        public String getTime()
        {
            return date.ToString("HH:mm");
        }
    }
}
