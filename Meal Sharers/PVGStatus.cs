﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers
{
    public class PVGStatus
    {
        private enum CookPVGStatus { AWAITING_RESULTS, OK, REJECTED };
        private CookPVGStatus cookPVGStatus;

        public PVGStatus()
        {
            cookPVGStatus = CookPVGStatus.AWAITING_RESULTS;
        }

        public void eventClick(bool response)
        {
            switch (cookPVGStatus)
            {
                // if state is AWAITING_RESULTS, run the method that will determine if the user got accepted or not
                case CookPVGStatus.AWAITING_RESULTS: GetAccepted(response); break;

                //No other case needed. From that point, the application will end anyway.
            }
        }

        private void GetAccepted(bool response)
        {
            if (response)
                cookPVGStatus = CookPVGStatus.OK;
            if (!response)
                cookPVGStatus = CookPVGStatus.REJECTED;
        }

        override public string ToString()
        {
            return "State of PVG = " + cookPVGStatus.ToString();
        }
    }
}
