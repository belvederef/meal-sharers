﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Meal_Sharers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        //State diagram implementation
        private HygieneCertificateStatus cookHygieneCertificate;
        private PVGStatus cookPVG;


        public MainWindow()
        {
            InitializeComponent();
            //label1.Content = cookHygieneCertificate.ToString();
            //label2.Content = cookPVG.ToString();
        }

        private void button1_Click(object sender, RoutedEventArgs e)// click event
        {
            cookHygieneCertificate.eventClick();
            //label1.Content = cookHygieneCertificate.ToString();
        }

        private void button2_Click(object sender, RoutedEventArgs e)// click event
        {
            cookPVG.eventClick(true);
            //label2.Content = cookPVG.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}

