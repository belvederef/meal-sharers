﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Meal_Sharers
{
    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : Window
    {
        private Cook cook;
        private HygieneCertificateStatus a;


        public SignUp()
        {
            InitializeComponent();
            cook = new Cook();
            
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            cook.getHygieneCertificateStatus().eventClick();
            label1.Content = cook.getHygieneCertificateStatus().ToString();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            cook.getPVGStatus().eventClick(true);
            label2.Content = cook.getPVGStatus().ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            Close();
        }
    }
}
