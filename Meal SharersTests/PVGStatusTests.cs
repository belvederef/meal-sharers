﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Meal_Sharers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers.Tests
{
    [TestClass()]
    public class PVGStatusTests
    {
        [TestMethod()]
        public void PVG_create_status_test()
        {
            //First test: the user undergoes the PVG. The user's status should be updated accordingly

            ////Arrange and act
            //Undergo the PVG (that is when the object is created)
            PVGStatus PVG_status = new PVGStatus();
            String expected = "State of PVG = AWAITING_RESULTS";

            ////Assert
            Assert.AreEqual(expected, PVG_status.ToString(), "Incorrect status");
        }

        [TestMethod()]
        public void PVG_accepted_test()
        {
            //Undergone the PVG, get results. In this case, get accepted

            ////Arrange
            PVGStatus PVG_status = new PVGStatus();
            String expected = "State of PVG = OK";

            ////Act
            PVG_status.eventClick(true);

            ////Assert
            Assert.AreEqual(expected, PVG_status.ToString(), "Incorrect status");
        }

        [TestMethod()]
        public void PVG_rejected_test()
        {
            //Undergone the PVG, get results. In this case, get rejected

            ////Arrange
            PVGStatus PVG_status = new PVGStatus();
            String expected = "State of PVG = REJECTED";

            ////Act
            PVG_status.eventClick(false);

            ////Assert
            Assert.AreEqual(expected, PVG_status.ToString(), "Incorrect status");
        }
    }
}