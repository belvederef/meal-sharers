﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StateChartTests
{
    [TestClass]
    public class HygieneCertificateTest
    {
        [TestMethod]
        public void PositiveResult()
        {
            // arrange
            
            double beginningBalance = 11.99;
            double debitAmount = 4.55;
            double expected = 7.44;
            
            
            // act  
            account.Debit(debitAmount);

            // assert  
            double actual = account.Balance;
            Assert.AreEqual(expected, actual, 0.001, "Account not debited correctly");
        }
    }
}
