﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers
{
    public class HygieneCertificateStatus
    {
        private enum CookCertificateStatus { NONE, OK, RENEW };
        private CookCertificateStatus cookCertificateStatus;
        private DateTime certificateExpiry;

        public HygieneCertificateStatus(DateTime expiryDate)
        {
            certificateExpiry = expiryDate;
            cookCertificateStatus = CookCertificateStatus.NONE;
        }

        public void eventClick()
        {
            switch (cookCertificateStatus)
            {
                case CookCertificateStatus.NONE: Complete(); break; // if state is NONE, let the user complete the certificate
                case CookCertificateStatus.OK: Check(certificateExpiry); break; // if state is OK, check for its validity
                case CookCertificateStatus.RENEW: Renew(); break;  // if state is renewal within 3 months, send a notification and renew
            }
        }

        private void Complete()
        {
            cookCertificateStatus = CookCertificateStatus.OK;
        }

        private void Check(DateTime certificateExpiry)
        {
            if (DateTime.Today >= certificateExpiry.AddMonths(-3) && DateTime.Today < certificateExpiry)
                cookCertificateStatus = CookCertificateStatus.RENEW;
        }

        private void Renew()
        {
            cookCertificateStatus = CookCertificateStatus.OK;
        }

        public void addCertificateExpiryDate(DateTime expiryDate)
        {
            certificateExpiry = expiryDate;
        }
        override public string ToString()
        {
            return "State of Hygiene Certificate = " + cookCertificateStatus.ToString();
        }
    }
}
