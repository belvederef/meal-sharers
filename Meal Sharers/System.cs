﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meal_Sharers
{
    class System_
    {
        private List<Eater> eaters;
        private List<Cook> cooks;
        private List<Info> mealShareInfo;

        public System_() { }

        public void addCookToSystem(Cook cook)
        {
            cooks.Add(cook);
        }
        public void addEaterToSystem(Eater eater)
        {
            eaters.Add(eater);
        }
        public Eater selectEaterFromList(int id)
        {
            return eaters[id];
        }
        public void uploadPicture(Meal meal) { }
        public void uploadFeedback(String eaterFeedback, String cookFeedback) { }
    }
}
